package com.woniuxy.secondkill.controller;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.TimeoutUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 秒杀系统
 */
@RestController
public class KillController {

//    @Autowired
    @Resource
    private RedisTemplate<String,Object> redisTemplate;
    @Resource
    private Redisson redisson;

    @RequestMapping("/kill")
    public boolean kill(){
        //1.判断库存是否足够
            String key = "key";
            //获取锁
        RLock lock = redisson.getLock(key);
        try {
            lock.lock();
            int store = (Integer) redisTemplate.opsForValue().get("store");
            if (store>0){
                store--;
                redisTemplate.opsForValue().set("store", store);
                System.out.println("剩余库存"+store);
                System.out.println("最牛逼的李四提交的代码");
                System.out.println("最牛逼的张三提交的代码");

                System.out.println("李四分支的代码");

                System.out.println("在张三分支下编写的代码");
                return true;

            }
        }finally {
            lock.unlock();
        }
            //通过redis的setnx设置分布锁，判断是否有人在使用,设置过期时间防止服务器挂掉，锁未释放
            //为key加上唯一的val，方便判断是否为自己创建的key，防止错删其他线程创建的key
//            String val = UUID.randomUUID().toString();
//            Boolean flag = redisTemplate.opsForValue().setIfAbsent(key, val, 10, TimeUnit.SECONDS);
//            if(flag) {
//                try {
//                    int store = (Integer) redisTemplate.opsForValue().get("store");
//                    if (store > 0) {
//                        store--;
//                        System.out.println("秒杀成功，库存还剩" + store);
//                        redisTemplate.opsForValue().set("store", store);
//
//                        return true;
//                    }
//                }finally{
//                    String uuid = (String)redisTemplate.opsForValue().get(key);
//                        if (uuid.equals(val)){
//                            //对数据操作完成后，移除锁，不管是否抛异常都会释放锁
//                            redisTemplate.delete(key);
//                        }
//                    }
//            }
        return false;
    }
}
